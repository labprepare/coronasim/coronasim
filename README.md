- [Corona Sim](#corona-sim)
  - [Main Goal and Motivation](#main-goal-and-motivation)
  - [Setting](#setting)
  - [Technical Details](#technical-details)
  - [Current State](#current-state)
  - [Roadmap](#roadmap)
  - [Problems we encountered](#problems-we-encountered)
  - [Lessons learned](#lessons-learned)
  - [Further Information and useful Links:](#further-information-and-useful-links)
    - [Godot](#godot)
    - [Model](#model)
    - [Papers](#papers)
    - [Other Simulations](#other-simulations)


# Corona Sim
Because we mainly use [Gitlab](https://gitlab.com) for our code and our documentation the latest version is always [here](https://gitlab.com/labprepare/coronasim/coronasim/-/wikis/home).

## Main Goal and Motivation
The current coronavirus pandemic is impacting the living conditions of most people. Countries tried to slow the spread of the virus by implementing a variety of countermeasures. To give a better understanding on their effectiveness, we want to program a simulation game. We hope, that players will get a deeper understanding of how pandemics are spreading and how to defeat them. Hopefully, many who play our game will understand why some restrictions are neccessary in times like these.
For our simulation we want to be as close to real life as possible. By doing so we hope to maximize the learning effect and the relatability for our players.

## Setting
You (the player) are a mayor of a small city during a pandemic. As a mayor you want to get reelected. For that the response to the pandemic must be adequate or else the population loses their trust in your abilities. Your relatives also live in the City and when you make bad choices, they might suffer from the consequences.

## Technical Details
We used Godot as our game engine, partly because it seemed easy to pick up and supports deploying the game as a website. For our initial version we choosed GDScript as our language of choice.
Because Godot advertises GDScript (the inhouse developed ScriptLanguage) which has the best compatibility with Godot (Our initial choice was C#, but a note that the support was in beta made us switch).
Currently, we use C# our language, because it provides a better typed experience and support for cyclic dependencies, which is a requirement from our Design.
For information regarding the architecture of the simulation, have a look at [the Design Specification](https://gitlab.com/labprepare/coronasim/coronasim/-/wikis/Design).

## Current State
The project is (at the time of writing) heavily WIP. Therefore not all features are available and things might change. This being said, [the current version is online](https://labprepare.gitlab.io/coronasim/coronasim/).

Things that already (mostly ^^) work:
- Generating a random map every time.
- Generating a random population with families where each person has their own job and home address.
- Walking across the map including houses.

## Roadmap
There is still lots to do until we would consider the project to be in a minimum viable state. We set some Milestones to be our different versions of Prototypes, which can be found in [the Design Specification](https://gitlab.com/labprepare/coronasim/coronasim/-/wikis/Design). The second Prototype we wanted to include everything absolutely essential to deliver the core experience. After that there are still more ideas to improve upon, but (for now) that is something future us has to worry about.

## Problems we encountered
Working with a new programming language always has its challenges. For context, GDScript offers some support to add type hints, which can help with understanding what variables can be used in which ways. However, the support is very minimal and using the feature can also lead to [Cyclical Dependency Errors](https://en.wikipedia.org/wiki/Circular_dependency), which were a headache to resolve.

Another interesting problem is that the navigation that Godot provides does not work as expected. To give some context, the developer can specify areas where the characters will be able to walk on the map by placing polygons on the tiles.

![The characters are only supposed to be able to walk on the light blue area.](https://gitlab.com/labprepare/coronasim/coronasim/-/wikis/Pictures/NavigationPoligon.png)

When Godot builds the whole map by placing the tiles it can then figure out, where a person should be able to go, and how they can get there. However, in what seems to be an optimization effort, Godot ignores some of these polygons. This led to problems, because we wanted the characters to walk through the doors, however they just phased through the walls like a ghost, which, while interesting, is not something ordinary people can do.

Working with Godot revealed that it itself is still under development and several features that might be expected in a Game Engine are simply not developed yet. This caused several problems which we had to painstakenly resolve manually.

When making the design specification for the simulator, we knew that we wanted to use Godot, however we did not have any experience with it, so the resulting design had some places where it was incompatible with the architecture of Godot. This lead to us having to redo most of the specification, which took a couple of weeks, delaying the project.

## Lessons learned

- Only start doing the design specification of projects once the tools that will be used are understood well enough.
- GDScript (or immature languages in general) do not work well when trying to build complex systems.
- Developing a game is probably worth more than 3lp. ^^

## Further Information and useful Links:
### Godot

  - Godot Project Page: https://godotengine.org/
  - Documentation: https://docs.godotengine.org/en/stable/
  - Pathfinding in a Tilemap: https://godotengine.org/qa/1790/how-to-implement-pathfinding-with-a-tilemap
  - unofficial Godot C# API Documentation(currently offline): https://godotsharp.net/

### Model

  - Networksscience with focus on epidemics: http://networksciencebook.com/chapter/10#introduction10

### Papers

  - [Wirksamkeit nicht-pharmazeutischer Interventionen](https://gitlab.com/labprepare/coronasim/coronasim/uploads/e0ff2f07d5cb1884f5bf2e2138c5be96/Rapid-Review-NPIs.pdf)

### Other Simulations

  - https://github.com/cool-RR/grid_royale
  - 3Blue1Brown Simulations:
    - https://www.youtube.com/watch?v=gxAaO2rsdIs
    - https://www.youtube.com/watch?v=Kas0tIxDvrg
  - https://player.vimeo.com/video/6590604
  - https://covid-simulator.com/
  - Gleamviz predict the future based on real world data: http://www.gleamviz.org/
  - [TU COMA Homework](https://gitlab.com/labprepare/coronasim/coronasim/uploads/87e36b42135c0d2955f6d7b61826a9bd/Projekt.pdf)
  - https://isis.tu-berlin.de/course/view.php?id=18850
