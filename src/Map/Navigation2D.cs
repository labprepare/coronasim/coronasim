
using System;
using System.Collections.Generic;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using Array = Godot.Collections.Array;


public class Navigation : Navigation2D
{

	public Dictionary doorPositions = new Dictionary(){};
	public TileMap tileMap;

    Navigation() {
        tileMap = GetNode<TileMap>("./TileMap");
    }

    public void SetDoors(Dictionary positions)
	{
		doorPositions = positions;

	}

	public Vector2[] FindPath(Vector2 start, Vector2 target)
	{
		Vector2[] path;
		// Vector2 targetMapPos = tileMap.WorldToMap(target);
		// //world position of the cell that was clicked
		// Vector2 targetWorldPos = tileMap.MapToWorld(targetMapPos);
		// //get tile index
		// int targetID = tileMap.GetCellv(targetMapPos);

		// Vector2 startMapPos = tileMap.WorldToMap(start);
		// Vector2 startWorldPos = tileMap.MapToWorld(startMapPos);
		// int startID = tileMap.GetCellv(startMapPos);

		// if(CheckForDoor(targetID) && targetMapPos != startMapPos)
		// {
		// 	var target_door = GetDoorOutside(targetID, targetWorldPos);
		// 	if(CheckForDoor(startID))
		// 	{
		// 		path = GetSimplePath(start, GetDoorInside(startID, startWorldPos));
		// 		var start_door = GetDoorOutside(startID, startWorldPos);
		// 		path.AppendArray(GetSimplePath(start_door , target_door));
		// 	}
		// 	else
		// 	{
		// 		path = GetSimplePath(start, target_door);
		// 	}
		// 	path = path + GetSimplePath(GetDoorInside(targetID, targetWorldPos), target);
		// }
		// else if(CheckForDoor(startID) && startMapPos != targetMapPos)
		// {
		// 	var start_door = GetDoorInside(startID, startWorldPos);
		// 	path = GetSimplePath(start, start_door);
		// 	path = path + GetSimplePath(GetDoorOutside(startID, startWorldPos), target);
		// }
		// else
		// {
		// }
		path = this.GetSimplePath(start, target);
		return path;

	}

	public bool CheckForDoor(int tileId)
	{
		if(tileId == -1)
		{
			return false;
		}
		else if(doorPositions.Contains(tileId))
		{
			return true;
		}
		return false;

	}

	/*public Vector2 GetDoorInside(int tileId, Vector2 tilePos)
	{
		Array doorPos = doorPositions[tileId];
		return doorPos[0] + tilePos;

	}

	public Vector2 GetDoorOutside(int tileId, Vector2 tilePos)
	{
		Array doorPos = doorPositions[tileId];
		return doorPos[1] + tilePos;
	}
	public Vector2[] AppendArrays(Vector2[] array1, Vector2[] array2)
	{
		List<Vector2> returnList = array1.ToList<Vector2>();
		foreach (Vector2 vector in array2.ToList<Vector2>())
		{
            returnList.Add(vector);
        }
		return returnList.ToArray();
	}*/



}