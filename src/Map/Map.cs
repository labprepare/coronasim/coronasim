using System;
using System.Collections.Generic;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using Array = Godot.Collections.Array;


public class Map : Node2D
{
    public Person protagonist;
    public List<Person> npcs;
    public Navigation2D navigation;
    public TileMap tilemap;
    public Population population;
    public Viewport viewport;
    public OnMapGraph onMapGraph;
    public Person firstInfected;
    public bool pressed = false;
    public bool playerControl = true;
    public Dictionary<Vector2, IMapObject> Buildings = new Dictionary<Vector2, IMapObject>();
    public override void _Ready()
    {

        navigation = GetNode<Navigation2D>("./Navigation2D");

        tilemap = GetNode<TileMap>("./Navigation2D/TileMap");

        population = GetNode<Population>("./Population");

        viewport = GetViewport();

        MapGenerator populator = new MapGenerator(this);

        populator.populateMap(population);

        protagonist = population.GetProtagonist();

        // protagonist.GetNode<Button>("./Button").Disabled = false;

        // protagonist.GetNode<Polygon2D>("./Polygon2D").Color = new Color(100, 20, 5);

        Disease disease = Disease.COVID19;

        TimeSpan timeSkip = TimeSpan.FromDays(3);

        firstInfected = population.AllPeople.GetRandom();
        
        firstInfected.OwnInfections.Add(new Infection(null, firstInfected, disease, Time.GetTime() - timeSkip));

        firstInfected.GetNode<Polygon2D>("./Polygon2D").Color = new Color(100, 20, 5);

        npcs = population.AllPeople.FindAll((Person p) => p.GetId() != protagonist.GetId());

        Camera2D cam = protagonist.GetNode<Camera2D>("./Camera2D");

        cam.Current = true;


        // Camera2D camera = new Camera2D();
        // camera.AnchorMode = Camera2D.AnchorModeEnum.DragCenter;
        // protagonist.AddChild(camera);

        //SetDoorInformations();


        GetNode<DirectionalArrow>("./CanvasLayer/DirectionalArrow").protagonist = protagonist;

        //Time.AdvanceTime(timeSkip);

        //Time.SetSpeedCustom(10);
        cam.Zoom = new Vector2(3, 3);
    }

    // convert to _UnhandledInput?
    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("click"))
        {
            Vector2 mousePosition = GetGlobalMousePosition();
            Vector2[] path = navigation.GetSimplePath(protagonist.Position, mousePosition, optimize: false);
            protagonist.SetPath(path);
        }
        if (@event.IsActionPressed("pause"))
        {
            List<Vector2> vector = new List<Vector2>();
            protagonist.Path = vector;
        }
        if (@event.IsActionPressed("change_control"))
        {
            playerControl = !playerControl;
        }
        if (@event.IsActionPressed("zoom_in"))
        {
            Camera2D camera = protagonist.GetNode<Camera2D>("./Camera2D");
            camera.Zoom -= new Vector2((float)0.5, (float)0.5);
        }
        if (@event.IsActionPressed("zoom_out"))
        {
            Camera2D camera = protagonist.GetNode<Camera2D>("./Camera2D");
            camera.Zoom += new Vector2((float)0.5, (float)0.5);
        }
    }
    public void _onButtonPressed()
    {
        if (pressed)
        {
            onMapGraph.ResetButtonPressed();
            pressed = false;
        }
        else
        {
            onMapGraph = new OnMapGraph(population, protagonist, protagonist);
            pressed = true;
        }

    }

    public void PlaceMapObject(Vector2 Position, IMapObject MapObject)
    {
        tilemap.SetCellv(Position, tilemap.TileSet.FindTileByName(MapObject.TileName));
        Buildings[Position] = MapObject;
    }

    public Dictionary<Direction, List<IMapObject>> Surroundings(IMapObject MapObject)
    {
        return MapObject.Neighboring().ConvertAll(vectorlist => vectorlist.ConvertAll(vec => Buildings.GetValue(vec)).FindAll(obj => obj != null));
    }

    public bool IsOccupied(Vector2 Position) => Buildings.GetValue(Position) != null;
    public IMapObject GetMapObject(Vector2 Position) => Buildings.GetValue(Position);
}
