using Godot;
using System;
using System.Collections.Generic;

public abstract class AssignableBuilding : Building
{
    public List<Person> People;
    public String Key;
    public AssignableBuilding(List<Person> people, String key)
    {
        People = people;
        Key = key;
    }
    public virtual void assignBuildingToPeople()
    {
        foreach (Person person in People)
        {
            person.AssignedBuildings[Key] = this;
        }
    }
}