using Godot;
using System;
using System.Collections.Generic;
using System.Linq;




public abstract class Building : TempHumanContainer, IMapObject
{
    //Building and MapObject share the following:

    protected List<BuildingVariant> Variants;
    protected BuildingVariant chosenVariant;
    public BuildingVariant ChosenVariant
    {
        get => chosenVariant;
    }

    public string TileName
    {
        get => ChosenVariant.TileName;
    }


    public abstract int Size { get; }

    private Vector2 _Position = new Vector2(0, 0);
    public Vector2 Position
    {
        get { _slots = null; return _Position; }
        set { _Position = value; }
    }
    public Vector2 BottomRightCorner
    {
        get => Position + new Vector2(Size - 1, Size - 1);
    }

    private List<Vector2> _slots = null;
    public List<Vector2> Slots()
    {
        if (_slots == null)
        {
            _slots = Util.GetFilledCoordinates(Position, BottomRightCorner);
        }
        return _slots;

    }

    public virtual Boolean chooseFittingVariantIfAvailiable(Dictionary<Direction, List<IMapObject>> surroundings)
    {
        Converter<IMapObject, Direction> getDirection = (IMapObject neighbor) => neighbor.ChosenVariant.Accessibility;
        Dictionary<Direction, List<Direction>> accessibilitiesByDirection = surroundings.ConvertAll(edgeNeighbors => edgeNeighbors.ConvertAll(getDirection));
        List<BuildingVariant> fittingVariants = accessibleVariants(accessibilitiesByDirection).FindAll(v => v.Size == Size);
        //Remove all Variants from the neighborhood.
        List<BuildingVariant> neighboringBuildingVariants =
            surroundings.Values.SelectMany(i=>i).ToList() //Reduce from Dict<List<MapObject>> to just List<MapObject>
            .ConvertAll(i=>i.ChosenVariant);
        fittingVariants = fittingVariants.FindAll(v=>!neighboringBuildingVariants.Contains(v));


        //Choose one of the Variants
        if (fittingVariants.Count == 0) return false;
        chosenVariant = fittingVariants.GetRandom();
        return true;
    }

    private List<BuildingVariant> accessibleVariants(Dictionary<Direction, List<Direction>> surroundings)
    {
        Direction accessors = Direction.None;
        foreach (var edge in surroundings)
        {
            foreach (var neighbor in edge.Value)
            {
                accessors |= (edge.Key & neighbor.Opposite());
            }
        }
        return Variants.FindAll(v => (v.Accessibility & accessors) != 0);
    }

    public Dictionary<Direction, List<Vector2>> Neighboring(Direction direction = Direction.All)
    {
        return Util.GetDirectionalBorder(Position + new Vector2(-1, -1), BottomRightCorner + new Vector2(1, 1));
    }
}