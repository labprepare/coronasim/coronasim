using Godot;
using System;
using System.Collections.Generic;

public class Office : AssignableBuilding
{
    public Office(List<Person> employees) : base(employees, "office") {
        Variants = new List<BuildingVariant> {
            new BuildingVariant("office_1",2,Direction.South),
            new BuildingVariant("office_2",2,Direction.North)
        };
    }

    public override int Size => 2;
}