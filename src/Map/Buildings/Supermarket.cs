using Godot;
using System;
using System.Collections.Generic;

public class Supermarket : Building
{
    public Supermarket()
    {
        Variants = new List<BuildingVariant> {
            new BuildingVariant("supermarket_1",2,Direction.South),
            new BuildingVariant("supermarket_2",2,Direction.North)
        };
    }
    public override int Size => 2;

}