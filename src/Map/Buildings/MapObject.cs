using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public interface IMapObject
{
	BuildingVariant ChosenVariant
	{
		get;
	}
	Vector2 Position
	{
		get;
		set;
	}
	int Size
	{
		get;
	}
	Vector2 BottomRightCorner
	{
		get;
	}
	List<Vector2> Slots();

	string TileName
	{
		get;
	}

	Boolean chooseFittingVariantIfAvailiable(Dictionary<Direction, List<IMapObject>> surroundings);
	Dictionary<Direction, List<Vector2>> Neighboring(Direction direction = Direction.All);
}

public abstract class MapObject : IMapObject
{
	//Building and MapObject share the following:

	protected List<BuildingVariant> Variants;
	protected BuildingVariant chosenVariant;
	public BuildingVariant ChosenVariant
	{
		get => chosenVariant;
	}

	public string TileName
	{
		get => ChosenVariant.TileName;
	}


	public abstract int Size { get; }

	private Vector2 _Position = new Vector2(0, 0);
	public Vector2 Position
	{
		get { _slots = null; return _Position; }
		set { _Position = value; }
	}
	public Vector2 BottomRightCorner
	{
		get => Position + new Vector2(Size - 1, Size - 1);
	}

	private List<Vector2> _slots = null;
	public List<Vector2> Slots()
	{
		if (_slots == null)
		{
			_slots = Util.GetFilledCoordinates(Position, BottomRightCorner);
		}
		return _slots;

	}

	public virtual Boolean chooseFittingVariantIfAvailiable(Dictionary<Direction, List<IMapObject>> surroundings)
	{
		Converter<IMapObject, Direction> getDirection = (IMapObject neighbor) => neighbor.ChosenVariant.Accessibility;
		Dictionary<Direction, List<Direction>> accessibilitiesByDirection = surroundings.ConvertAll(edgeNeighbors => edgeNeighbors.ConvertAll(getDirection));
		List<BuildingVariant> fittingVariants = accessibleVariants(accessibilitiesByDirection).FindAll(v => v.Size == Size);
		//Remove all Variants from the neighborhood.
		List<BuildingVariant> neighboringBuildingVariants =
			surroundings.Values.SelectMany(i => i).ToList() //Reduce from Dict<List<MapObject>> to just List<MapObject>
			.ConvertAll(i => i.ChosenVariant);
		fittingVariants = fittingVariants.FindAll(v => !neighboringBuildingVariants.Contains(v));


		//Choose one of the Variants
		if (fittingVariants.Count == 0) return false;
		chosenVariant = fittingVariants.GetRandom();
		return true;
	}

	private List<BuildingVariant> accessibleVariants(Dictionary<Direction, List<Direction>> surroundings)
	{
		Direction accessors = Direction.None;
		foreach (var edge in surroundings)
		{
			foreach (var neighbor in edge.Value)
			{
				accessors |= (edge.Key & neighbor.Opposite());
			}
		}
		return Variants.FindAll(v => (v.Accessibility & accessors) != 0);
	}

	public Dictionary<Direction, List<Vector2>> Neighboring(Direction direction = Direction.All)
	{
		return Util.GetDirectionalBorder(Position + new Vector2(-1, -1), BottomRightCorner + new Vector2(1, 1));
	}
	//Generated/Helper Methods
	public override bool Equals(object obj)
	{
		// If the passed object is null
		if (obj == null)
		{
			return false;
		}
		// all MapObjects (Parks and Streets) are Equal, in the sense that
		// they do not require changes in regards to Infectious Bubbles.
		return obj is MapObject;
	}
	public override int GetHashCode()
	{
		int hashCode = -219325302;
		hashCode = hashCode * -1521134295 + Size.GetHashCode();
		hashCode = hashCode * -1521134295 + _Position.GetHashCode();
		hashCode = hashCode * -1521134295 + Position.GetHashCode();
		hashCode = hashCode * -1521134295 + EqualityComparer<List<Vector2>>.Default.GetHashCode(_slots);
		return hashCode;
	}
}

public class BuildingVariant
{
	public readonly String TileName;
	public readonly Direction Accessibility;
	public readonly int Size;
	public BuildingVariant() { }
	public BuildingVariant(String TileName, int Size, Direction Accessibility)
	{
		this.TileName = TileName;
		this.Size = Size;
		this.Accessibility = Accessibility;
	}

	public override bool Equals(object obj)
	{
		if (obj == null || GetType() != obj.GetType())
		{
			return false;
		}
		BuildingVariant other = (BuildingVariant)obj;
		return
			this.TileName == other.TileName &&
			this.Size == other.Size &&
			this.Accessibility == other.Accessibility
		;
	}

	public override int GetHashCode()
	{
		return (TileName.GetHashCode() * Size * (int)Accessibility).GetHashCode();
	}

}
