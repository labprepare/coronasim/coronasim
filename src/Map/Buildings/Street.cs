using System.Collections.Generic;
using System;

public class Street : MapObject
{
    public Street()
    {
        chosenVariant = new BuildingVariant("street", 1, Direction.All);
        Variants = new List<BuildingVariant> {
            new BuildingVariant("street",1,Direction.All),
        };
    }
    public override int Size => 1;
    public override Boolean chooseFittingVariantIfAvailiable(Dictionary<Direction, List<IMapObject>> surroundings) => true;
}