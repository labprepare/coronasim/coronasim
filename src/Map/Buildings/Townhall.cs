using Godot;
using System;
using System.Collections.Generic;

public class TownHall : Building
{

    public TownHall()
    {
        Variants = new List<BuildingVariant> {
            new BuildingVariant("townhall_1",2,Direction.South),
            new BuildingVariant("townhall_2",2,Direction.North)
        };
    }
    public override int Size => 2;
}