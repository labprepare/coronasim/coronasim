using Godot;
using System;
using System.Collections.Generic;

public class Park : MapObject
{
    public Park()
    {
        Variants = new List<BuildingVariant> {
            new BuildingVariant("park_1", 1, Direction.All),
            new BuildingVariant("park_2", 1, Direction.All),
            new BuildingVariant("park_3", 1, Direction.All),
        };
        chosenVariant = Variants.GetRandom();
    }

    public override int Size => 1;
    public override Boolean chooseFittingVariantIfAvailiable(Dictionary<Direction, List<IMapObject>> surroundings) => true;
}