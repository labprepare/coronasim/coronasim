using System;
using Godot;

[Flags]
public enum Direction
{
    None  = 0b_0000,

    North = 0b_1000,
    East  = 0b_0100,
    South = 0b_0010,
    West  = 0b_0001,

    XAxis = East | West,
    YAxis = North | South,
    All = North | South | East | West
}

static class DirectionMethods
{
    public static Direction Opposite(this Direction s1)
    {
        Direction result = Direction.None;
        if ((s1&Direction.North)!=0){ result |= Direction.South;}
        if ((s1&Direction.South)!=0){ result |= Direction.North;}
        if ((s1&Direction.East)!=0){result |= Direction.West;}
        if ((s1&Direction.West)!=0){result |= Direction.East;}

        return result;
    }

    // Make a bitflip on the direction: 0001 (North) -> 1110 (East,South,West)
    public static Direction Invert(this Direction s1) => ~s1;
}
