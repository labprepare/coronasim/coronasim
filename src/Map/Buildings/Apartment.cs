using Godot;
using System;
using System.Collections.Generic;

public class Apartment : AssignableBuilding
{
    enum EApartmentType
    {
        SMALL=1,
        BIG=2
    }

    private EApartmentType ApartmentType;

    public override int Size => (int) ApartmentType;

    public Apartment(List<Person> inhabitants) : base(inhabitants, "home")
    {
        Variants = new List<BuildingVariant> {
            new BuildingVariant("apartment_1",1,Direction.South),
            new BuildingVariant("apartment_2",1,Direction.South),
            new BuildingVariant("apartment_3",1,Direction.North),
            new BuildingVariant("apartment_big_1",2,Direction.South),
            new BuildingVariant("apartment_big_2",2,Direction.North),
        };
        ApartmentType = People.Count > 3 ? EApartmentType.BIG : EApartmentType.SMALL;
    }



    public void placePeopleInApartment(TileMap tilemap)
    {
        foreach (Person Person in People)
        {
            PersonEnters(Person);
            Person.Position = tilemap.MapToWorld(Position) + (tilemap.CellSize / 2);
        }
    }

}
