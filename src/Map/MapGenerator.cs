using System;
using Godot;
using System.Collections.Generic;
using Dictionary = Godot.Collections.Dictionary;

public class MapGenerator : Reference
{


    //Constant paramenters
    //Map size/layout options:
    private static Vector2 BLOCK_DIMENSIONS = new Vector2(5, 2);
    //Map composition options:
    private const float SUPERMARKET_FEQUENCY = 0.001f; // f stands for float ;)
    private const float OFFICES_FEQUENCY = 0.04f;
    private const float PARKS_FEQUENCY = 0.005f;
    private const int SUPERMARKET_OFFSET = 10;
    private const int OFFICES_OFFSET = 20;
    private const int PARKS_OFFSET = 50;

    private const int LOOKAHEAD = 3;
    private BlockIterator currentBlock;
    private Map Map;
    private TileMap TileMap;
    private MapBlock CurrentMapBlock;

    private List<IMapObject> ObjectsToPlaceCache = null;

    public MapGenerator(Map Map)
    {
        this.Map = Map;
        TileMap = Map.tilemap;
        currentBlock = new BlockIterator();
        NextBlock();
    }

    public void populateMap(Population population)
    {

        List<IMapObject> objectsToPlace = CreateListOfObjects(population);

        PlaceObjects(objectsToPlace.GetRange(0, objectsToPlace.Count));

        assignBuildings(objectsToPlace);

        Map.tilemap.UpdateBitmaskRegion();
        //TileMap.UpdateBitmaskRegion();
    }

    private void assignBuildings(List<IMapObject> mapObjects)
    {
        foreach (IMapObject building in mapObjects)
        {
            // when it is an appartment building, attach the people that live there to it.
            AssignableBuilding assignableBuilding = building as AssignableBuilding;
            if (assignableBuilding != null)
            {
                assignableBuilding.assignBuildingToPeople();
                //additionally, place people in their homes.
                Apartment apartment = assignableBuilding as Apartment;
                if (apartment != null)
                {
                    apartment.placePeopleInApartment(TileMap);
                }

            }
        }
    }



    private void NextBlock()
    {
        //if the old block is valid, fill the remaining fields with parks
        if (CurrentMapBlock != null)
        {
            CurrentMapBlock.FillRemaining();
        }
        CurrentMapBlock = new MapBlock(Map, currentBlock.Next());

        //takes objects out of buildings list && places them on the map
    }

    private void PlaceObjects(List<IMapObject> buildings)
    {
        List<IMapObject> cache = new List<IMapObject>();

        while (buildings.Count > 0)
        {
            // fill the cache with up to LOOKAHEAD many objects
            while (cache.Count < LOOKAHEAD && buildings.Count > 0)
            {
                cache.Add(buildings.PopFirst());
            }
            List<IMapObject> leftoverBuildings = new List<IMapObject>();
            Boolean placedAnyObject = false;
            //try to place the buildings inside of the current block

            foreach (IMapObject building in cache)
            {
                //try to fit building into block, if it does not fit, save it for later
                if (CurrentMapBlock.PlaceInBlock(building))
                {
                    placedAnyObject = true;
                }
                else
                {
                    leftoverBuildings.Add(building);
                }
            }
            // if no buildings were placed, fill the remainding tiles with park
            if (!placedAnyObject && leftoverBuildings.Count != 0)
            {

                NextBlock();
            }
            //take the leftover buildings to the next round
            cache = leftoverBuildings;


        }
        //Fill the last Map block.
        CurrentMapBlock.FillRemaining();

        CreateBorder();

    }

    private void CreateBorder()
    {
        foreach (var blockCord in currentBlock.getBorderBlocks())
        {
            CurrentMapBlock = new MapBlock(Map, blockCord, true);
        }
    }

    private List<IMapObject> CreateListOfObjects(Population population)
    {
        List<IMapObject> objectsToPlace = new List<IMapObject>();
        //Apartments
        List<Apartment> appartmentAssignments = population.GetAppartmentAssignments();
        objectsToPlace.AddRange(appartmentAssignments);
        //Supermarkets
        int supermarketCount = (int)(Mathf.Round(Population.POPULATION_SIZE * SUPERMARKET_FEQUENCY));
        supermarketCount = supermarketCount + SUPERMARKET_OFFSET;
        foreach (int _i in GD.Range(supermarketCount))
        {
            objectsToPlace.Add(new Supermarket());

        }
        //Offices
        int officesCount = (int)(Mathf.Round(Population.POPULATION_SIZE * OFFICES_FEQUENCY + OFFICES_OFFSET));
        foreach (int _i in GD.Range(officesCount))
        {
            objectsToPlace.Add(new Office(new List<Person>() { }));

        }
        //Parks
        int parksCount = (int)(Mathf.Round(Population.POPULATION_SIZE * PARKS_FEQUENCY + PARKS_OFFSET));
        foreach (int _i in GD.Range(parksCount))
        {
            objectsToPlace.Add(new Park());

        }
        // Shuffle List
        objectsToPlace.Shuffle();


        // townhall shall be the first object that needs to be placed
        TownHall townHall = new TownHall();
        objectsToPlace.Insert(0, townHall);
        return objectsToPlace;
    }

    private class BlockIterator
    {
        private int layer;
        private List<Vector2> currentLayer;
        public BlockIterator()
        {
            layer = 0;
            currentLayer = new List<Vector2> { new Vector2(0, 0) };

        }

        private void NextLayer()
        {
            layer = layer + 1;
            currentLayer = GenerateCoodinates();
            currentLayer.Shuffle();

        }

        private List<Vector2> GenerateCoodinates()
        {
            //generates the coordinates of the layer-th ring around (0,0). layer>0
            System.Diagnostics.Debug.Assert(layer > 0, "layers ERROR>0 did not hold.");
            return Util.GetBorderCoordinates(new Vector2(-layer, -layer), new Vector2(layer, layer));


        }

        public Vector2 Next()
        {
            if (currentLayer.Count == 0)
            {
                NextLayer();
            }
            return (Vector2)currentLayer.PopFirstN();

        }

        public List<Vector2> getBorderBlocks()
        {
            for (int i = 0; i < 3; i++)
            {
                layer = layer + 1;
                currentLayer.AddRange(GenerateCoodinates());
            }
            return currentLayer;
        }

    }

    private class MapBlock
    {
        private IMapObject borderBuilding() => new Street();
        private IMapObject alternativeBuilding() => new Park();

        //private TileMap tilemap;
        private Map Map;
        private Vector2 blockCords;

        private Vector2 streetBlockStart;
        private Vector2 streetBlockEnd;
        private Vector2 buildingsBlockStart;
        private Vector2 buildingsBlockEnd;

        private List<Vector2> buildingSlots;
        private List<Vector2> borderSlots;


        public MapBlock(Map Map, Vector2 cords, bool makeUnpassable = false)
        {
            this.Map = Map;
            //tilemap = tileMap;
            blockCords = cords;
            _SetupCords();
            buildingSlots = Util.GetFilledCoordinates(buildingsBlockStart, buildingsBlockEnd);
            borderSlots = Util.GetBorderCoordinates(streetBlockStart, streetBlockEnd);
            if (makeUnpassable)
            {
                MakeUnpassable();
            }
            else
            {
                CreateBorder();
            }
        }


        private void _SetupCords()
        {
            Vector2 offset = new Vector2(1, 1);
            streetBlockStart = blockCords * (BLOCK_DIMENSIONS + 1 * offset);
            //streetBlockStart = blockCords*BLOCK_DIMENSIONS;
            buildingsBlockStart = streetBlockStart + offset;
            buildingsBlockEnd = buildingsBlockStart + (BLOCK_DIMENSIONS - offset);
            streetBlockEnd = buildingsBlockEnd + offset;
        }

        private void CreateBorder()
        {
            foreach (Vector2 pos in borderSlots)
            {
                //tilemap.SetCellv(pos, tilemap.TileSet.FindTileByName(borderBuilding.TileName));
                IMapObject mapObject = borderBuilding();
                mapObject.Position = pos;
                Map.PlaceMapObject(pos, mapObject);
            }
        }

        public void FillRemaining()
        {
            while (PlaceInBlock(alternativeBuilding()))
            {
                //will try to place the building in the block. The return value idicates success.
            }
        }

        private void MakeUnpassable()
        {
            foreach (var coord in Util.GetFilledCoordinates(streetBlockStart, streetBlockEnd))
            {
                if (Map.tilemap.GetCellv(coord) == -1)
                {
                    Map.tilemap.SetCellv(coord, Map.tilemap.TileSet.FindTileByName("border_tile"));
                }
            }

        }

        public bool PlaceInBlock(IMapObject building)
        {
            Vector2? topLeftCorner = FindPlaceInBlock(building);

            if (topLeftCorner == null)
            {
                return false;
            }
            else
            {
                Dictionary<Direction, List<IMapObject>> surroundings = Map.Surroundings(building);
                Boolean hasFittingVariant = building.chooseFittingVariantIfAvailiable(surroundings);
                if (hasFittingVariant)
                {

                    building.Position = (Vector2)topLeftCorner;
                    foreach (Vector2 slot in building.Slots())
                    {
                        Map.PlaceMapObject(slot, building);
                    }
                    return true;
                    //tries to find a place where the building will fit. If there is no space, it returns null

                }
                return false;

            }
        }

        private Nullable<Vector2> FindPlaceInBlock(IMapObject building)
        {
            foreach (Vector2 slot in buildingSlots)
            {
                if (BuildingDoesFitIntoSlot(building, slot))
                {
                    return slot;
                }
            }
            return null;
        }

        // determines, if the given building will fit into the specified location.
        private bool BuildingDoesFitIntoSlot(IMapObject building, Vector2 topLeftCorner)
        {
            System.Diagnostics.Debug.Assert(building != null, "Building was null");
            // look at every slot that would be occupied by the building && check if it is
            Vector2 oldpos = building.Position;
            // if there is no place to place the building then restore old value.
            building.Position = topLeftCorner;
            foreach (Vector2 slot in building.Slots())
            {
                if (Map.IsOccupied(slot))
                {
                    building.Position = oldpos;
                    return false;
                }
            }
            return true;
        }
    }
}