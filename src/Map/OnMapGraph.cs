using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using Array = Godot.Collections.Array;
using System.Collections.Generic;

public class OnMapGraph : Reference
{
    
	public Population population;
	public Person person;
	public Person protagonist;
	public List<Person> infected;
	public DateTime time;
	public Button resetButton;


    public OnMapGraph(Population pop, Person firstInfected, Person mainPerson)
    {
		population = pop;
		person = firstInfected;
		protagonist = mainPerson;
		ShowGraphOnMap(person);
    }
	public void ResetButtonPressed()
	{
		resetPerson(person);
	}	
	public void ShowGraphOnMap(Person person)
	{
		time = Time.GetTime();
		Time.SetSpeedPause();
		BuildGraphOnMap(person);
	}
	public void BuildGraphOnMap(Person person)
	{
		Person infector = person.OwnInfections[0].PersonA;
		person.GetNode<Button>("./Button").Disabled = false;
		if(infector != null)
		{
			Line2D line = new Line2D();
			infector.AddChild(line);
			infector.GraphLines.Add(line);
			line.AddPoint(new Vector2(0, 0));
			line.AddPoint(person.Position - infector.Position);
			line.ShowBehindParent = true;
			line.Width = 2;
		}
		foreach (Infection infection in person.TransmittedInfections)
		{
			BuildGraphOnMap(infection.PersonB);
		}
	}
	public void reset(Person firstInfected)
	{
		resetPerson(firstInfected);
		Time.SetGameTime(time);
		Time.SetSpeedNormal();
	}
	public void resetPerson(Person person)
	{
		foreach (Line2D line in person.GraphLines)
		{
			line.RemoveAndSkip();
		}

		if (person.OwnInfections[0].PersonA != null)
		{
			person.OwnInfections[0].PersonA.RemoveChild(person);
			population.AddChild(person);
		}
		person.GetNode<Button>("./Button").Disabled = true;
		foreach (Infection infection in person.TransmittedInfections)
		{
			resetPerson(infection.PersonB);
		}
		
	}
}

