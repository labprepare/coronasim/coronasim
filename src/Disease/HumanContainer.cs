
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class TempHumanContainer : HumanContainer
{
    public override int Volume() => 750000;

    protected override float RespiratoryRate() => 10;

    protected override float SpeakingBreathingRatio() => 0.15F;

    protected override float VentillationRate() => 2;
}

public abstract class HumanContainer
{
    private List<Person> People = new List<Person>();
    private TimeTracker timeTracker = new TimeTracker();

    //returns the amount people breathe in there
    protected abstract float RespiratoryRate();
    //returns the percentage of speaking, must be in [0,1]
    protected abstract float SpeakingBreathingRatio();
    //returns the amount of times per hour the entire air in the room will be replaced per hour
    protected abstract float VentillationRate();
    // returns the Volume of the Simulated Space
    public abstract int Volume();

    public void PersonEnters(Person person)
    {
        //GD.Print(String.Format("Person {0} enterred room", person.Name));
        Simulate();
        People.Add(person);
    }

    public void PersonLeaves(Person person)
    {
        //GD.Print(String.Format("Person {0} left room",person.Name));
        Simulate();
        People.Remove(person);
    }

    private void AddAttendeeWithoutSimulating(Person person)
    {
        People.Add(person);
    }

    private void RemoveAttendeeWithoutSimulation(Person person)
    {
        People.Remove(person);
    }

    private void UpdateAttendees(List<Person> persons)
    {
        Simulate();
        People = persons;
    }

    protected void UpdateAttendeesWithoutSimulation(List<Person> persons)
    {
        People = persons;
    }

    private List<Person> InfectedPeoplePresent(Disease disease)
    {
        return People.FindAll(p => p.GetSpreadingDiseases().Contains(disease));
    }

    private List<Person> SusceptiblePeoplePresent(Disease disease)
    {
        return People.FindAll(p => p.IsSusceptibleAgainst(disease));
    }

    private List<Disease> DiseasesInCirculation()
    {
        return People.ConvertAll(p => p.GetSpreadingDiseases()).SelectMany(i => i).Distinct().ToList();
    }

    public void Simulate()
    {
        TimeSpan duration = timeTracker.GetInterval();
        //simulate the room since the last update
        foreach (Disease disease in DiseasesInCirculation())
        {

            List<Person> infected_people = InfectedPeoplePresent(disease);
            List<Person> susceptible_people = SusceptiblePeoplePresent(disease);
            //calculate the statistical probabilities:

            float mask_efficiency = 0;

            //Probability of Individual Being Infected
            float pibi = disease.ProbabilityOfIndividualBeingInfected(infected_people.Count,
                SpeakingBreathingRatio(),
                RespiratoryRate(),
                VentillationRate(),
                mask_efficiency,
                Volume(),
                duration);
            //GD.Print(pibi);

            //GD.Print(String.Format("Simulating Human Container:\nDisease: {0}\nInfected ({1}):{2}\n Susceptible ({3}):{4}\n PIBI:{5}", disease, infected_people.Count, infected_people, susceptible_people.Count, susceptible_people, pibi));

            //generate concrete numbers:
            foreach (Person victim in susceptible_people)
            {
                if (pibi > GD.Randf())
                {
                    Person spreader = infected_people.GetRandom();
                    // generate an infection originating from the spreader to the victim
                    spreader.GetInfection(disease).GenerateInfection(victim, Time.GetTime());
                }
            }
        }
    }
}
