
using System;
using Godot;
using MathNet.Numerics.Distributions;

public class Disease : Node
{
    /// <summary>
    /// Valid values: [0,1].
    /// The Probability of Phase 3 (Infectious  Symptomatic) happening for an Infection of this disease.
    /// </summary>
    private float Phase3Probability = 0.6f;
    /// <summary>
    /// Valid values: α ≥ 0.
    /// α  Value for the Gamma Distribution that controls Latency (Phase 1 Duration (Non Infectious Non Symptomatic)).
    /// </summary>
    private float LifetimeInAerosol = 1.7f;
	/// <summary>
    /// The Gamma Distribution for the Latenz Time
    /// </summary>
	private Gamma LatenzGamma = new Gamma(30f,10f,Util.RNG);
	/// <summary>
    /// The Gamma Distribution for the Incubation Time
    /// </summary>
	private Gamma IncubationGamma = new Gamma(6f,1.2f, Util.RNG);
	/// <summary>
    /// The Gamma Distribution for the Symptomatic Time
    /// </summary>
	private Gamma SymptomaticGamma = new Gamma(22f,2f, Util.RNG);
    /// <summary>
    /// Valid values: [0,1].
    /// The Probability that an inhaled Aerosol Droplet will deposit in the lungs.
    /// </summary>
    private float DepositionProbability = 0.5f;
    /// <summary>
    /// Valid values: X ≥ 0.
    /// Amount of Aerosol Particles per cm^3 when breathing.
    /// </summary>
    private float EmmisionBreathing = 0.06f;
    /// <summary>
    /// Valid values: X ≥ 0.
    /// Amount of Aerosol Particles per cm^3 when speaking.
    /// </summary>
    private float EmmisionSpeaking = 0.6f;
    /// <summary>
    /// Valid values: [0,1].
    /// The Probability of getting infected, when being exposed to a single particle.
    /// </summary>
    private float InfectionProbabilityPerParticle = 0.0022f;
    /// <summary>
    /// It's the Name?!
    /// </summary>
    public string DiseaseName = "Unnamed Virus";
    /// <summary>
    /// Valid values: X ≥ 0. (in what unit?)
    /// The size of the Aerosol Particles.
    /// </summary>
    private float AerosolDiameter = 5;
    /// <summary>
    /// Valid values: X ≥ 0.
    /// The amount of Particles in a mL of Respiratory fluid.
    /// </summary>
    float RNAConcentrationInRespiratoryFluid = 500000000;

    public static Disease COVID19 = new Disease
    {
        DiseaseName = "Covid19"
	};

	public static Disease COVID20 = new Disease{
			Phase3Probability = 1f,
			LatenzGamma= new Gamma(0.1f,1f), //nearly no latenz time, goes always in symptomatic
			IncubationGamma= new Gamma(6f,1.2f),
			SymptomaticGamma= new Gamma(22f,2f),
			LifetimeInAerosol= 24,
			DepositionProbability= 1,
			EmmisionBreathing= 0.06f,
			EmmisionSpeaking= 0.6f,
			InfectionProbabilityPerParticle= 1,
			DiseaseName= "COVID20"
	};
    public Disease() { }


	public float AerosolEmission(float speakBreathRatio,float breathingRate)
	{
		return (EmmisionBreathing * (1-speakBreathRatio) + EmmisionSpeaking * speakBreathRatio)*1000*60*breathingRate;

	}

	public float AerosolConcentration(float speakBreathRatio, float breathingRate, float volume)
	{
		return  AerosolEmission(speakBreathRatio,breathingRate) / volume;

	}

	public float RNAContentInAerosol()
	{
		return RNAConcentrationInRespiratoryFluid * Convert.ToSingle(Math.PI)/6 * AerosolDiameter; // PI is double so we could up that to double precision

	}

	public float RNAPerMin(float speakBreathRatio,float breathingRate,float volume)
	{
		return (breathingRate *
			AerosolConcentration(speakBreathRatio,breathingRate,volume) *
			RNAContentInAerosol() * DepositionProbability);

	}

	public float ParticlesPerMin(float speakBreathRatio,
		float breathingRate,
		float ventillationRate,
		float volume)
	{
		return (RNAPerMin(speakBreathRatio,breathingRate,volume))/(ventillationRate + 1/LifetimeInAerosol);

	}

	public float DosisInfectiousEpisode(int numberOfInfected,
		float speakBreathRatio,
		float breathingRate,
		float ventillationRate,
		float maskEfficiency,
		float volume,
		TimeSpan duration)
	{
		return (ParticlesPerMin(speakBreathRatio, breathingRate, ventillationRate, volume) *
			(float) duration.TotalMinutes *
			( 1 - maskEfficiency ) *
			numberOfInfected );
	}

	public float ProbabilityOfIndividualBeingInfected(int numberInfected,
		float speakBreathRatio,
		float breathingRate,
		float ventillationRate,
		float maskEfficiency,
		float volume,
		TimeSpan duration)
	{
		//return 1
		return 1 - Mathf.Pow(1-InfectionProbabilityPerParticle,
			DosisInfectiousEpisode(numberInfected,
				speakBreathRatio,
				breathingRate,
				ventillationRate,
				maskEfficiency,
				volume,
				duration));
	}

	public float Phase1Duration()
	{
		return (float)LatenzGamma.Sample();
	}

	public float Phase2Duration()
	{
		return (float)IncubationGamma.Sample();
	}

	public float Phase3Duration()
	{
		if (Util.RNG.NextDouble()>Phase3Probability)
		{
			return 0;
		}
		return (float)SymptomaticGamma.Sample();
	}
}
