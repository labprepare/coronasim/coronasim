
using System;
using Godot;


public class Infection : Edge
{
    private enum Phase
    {
        NON_INFECTIOUS_NON_SYMPTOMATIC,
        INFECTIOUS_NON_SYMPTOMATIC,
        INFECTIOUS_SYMPTOMATIC,
        RECOVERED
    }

    private Person Source => PersonA; // Source is the person that gave the Disease to Self
    private Person Self => PersonB; // Self is the Person this Infection is associated with.
    public Disease Disease;
    public DateTime TimeOfInfection;
    // (Non) Infectious (Non) Symptomatic
    public float NINSPhaseDuration;
    public float INSPhaseDuration;
    public float ISPhaseDuration;
    public float Severity;

    private TimeTracker timeSinceInfection;



    public Infection(Person source, Person self, Disease disease, DateTime time) : base(source, self)
    {
        // TODO take a look
        Disease = disease;
        TimeOfInfection = time;
        NINSPhaseDuration = Disease.Phase1Duration();
        INSPhaseDuration = Disease.Phase2Duration();
        ISPhaseDuration = Disease.Phase3Duration();
        timeSinceInfection = new TimeTracker();
        timeSinceInfection.SetLastTime(TimeOfInfection);
        //GD.Print(NINSPhaseDuration,";",INSPhaseDuration,";",ISPhaseDuration);
    }

    public void GenerateInfection(Person target, DateTime time)
    {
        // this infection:               (PersonA -> PersonB) -> target
        // the newly generated Infection: PersonA -> (PersonB -> target)
        Infection infection = new Infection(Self, target, this.Disease, time);
        // assign this person to be the origin of the new infection
        Self.TransmittedInfections.Add(infection);
        // generate the infection for the other person && assign it to them
        target.OwnInfections.Add(infection);
        target.GetNode<Polygon2D>("./Polygon2D").Color = new Color(100, 20, 5);
    }

    private Phase GetPhase()
    {
        //return Phase.INFECTIOUS_SYMPTOMATIC;

        TimeSpan TimeSinceInfection = timeSinceInfection.GetInterval(false);
        double Days = TimeSinceInfection.TotalDays;
        //GD.Print($"Days since Infection: {Days}\n NINS: {NINSPhaseDuration} \n  INS: {INSPhaseDuration} \n  I S: {ISPhaseDuration}");
        if (Days <= NINSPhaseDuration)
            return Phase.NON_INFECTIOUS_NON_SYMPTOMATIC;
        else if (Days <= INSPhaseDuration)
            return Phase.INFECTIOUS_NON_SYMPTOMATIC;
        else if (Days <= ISPhaseDuration)
            return Phase.INFECTIOUS_SYMPTOMATIC;
        else
            return Phase.RECOVERED;
    }

    public bool IsContagious()
    {
        Phase phase = GetPhase();
        bool v = phase == Phase.INFECTIOUS_NON_SYMPTOMATIC ||
                 phase == Phase.INFECTIOUS_SYMPTOMATIC;
        return v;
    }
    public bool IsRecovered()
    {
        Phase phase = GetPhase();
        return phase == Phase.RECOVERED;
    }
    public bool IsContagiousOrRecovered()
     {
        Phase phase = GetPhase();
        bool v = phase == Phase.INFECTIOUS_NON_SYMPTOMATIC ||
                 phase == Phase.INFECTIOUS_SYMPTOMATIC ||
                 phase == Phase.RECOVERED;
        return v;
    }

    public bool providesImmunity(Disease disease)
    {
        return disease == Disease;
    }
    public bool IsDeadly()
    {
        return Severity > 1;
    }
}
