using Godot;
using System;
public class InfectiousBubbleContainer : HumanContainer
{
	public InfectiousBubbleContainer() { }
	public InfectiousBubbleContainer(Person origin)
	{
		PersonEnters(origin);
	}

	public override int Volume() => 750000;

	protected override float RespiratoryRate() => 10;

	protected override float SpeakingBreathingRatio() => 0.15F;

	protected override float VentillationRate() => 50;
}


public class InfectiousBubble : Node2D
{
	private Person Person;
	private InfectiousBubbleContainer container;

	private Area2D InfectOthersArea;
	private CPUParticles2D Particles;
	private CollisionShape2D SmallBubble;


	private bool _Active;
	public bool Active
	{
		get => _Active;
		set
		{
			//GD.Print(value?"Activate Infectious Bubble":"Deactivate Infectious Bubble");
			if (value && Person.IsInfectious())
				container = new InfectiousBubbleContainer();
			Particles.Emitting = value && Person.IsInfectious();
			InfectOthersArea.Monitoring = value && Person.IsInfectious();
			SmallBubble.Disabled = !value;
			_Active = value;
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Person = (Person)GetParent();
		InfectOthersArea = GetNode<Area2D>("./Area2D");
		Particles = GetNode<CPUParticles2D>("./Particles");
		SmallBubble = GetNode<CollisionShape2D>("./KinematicBody2D/CollisionShape2D");

		Active = false;
	}
	private void InfectiousAreaBodyChange(object body, bool entered)
	{
		if (body is KinematicBody2D && ((KinematicBody2D)body).GetParent() is InfectiousBubble)
		{
			InfectiousBubble other = (InfectiousBubble)((KinematicBody2D)body).GetParent();
			Person person = other.Person;
			if (entered)
			{
				container.PersonEnters(person);
			}
			else
			{
				container.PersonLeaves(person);
			}
		}
	}
}
