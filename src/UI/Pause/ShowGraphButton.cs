using Godot;
using System;

public class ShowGraphButton : Control
{
    public GraphVisualization graph;
    private void _on_Button_pressed()
	{
		//Map map = GetParent<PauseMenu>().GetParent<Control>().GetParent<CanvasLayer>().GetParent<Node2D>().GetNode<Map>("./Map");
		Map map = GetViewport().GetNode<Map>("./Node2D/Map");
		PackedScene GraphScene = (PackedScene)ResourceLoader.Load("res://src/Population/GraphVisualization/GraphVisualization.tscn");
		graph = GraphScene.Instance<GraphVisualization>();
		graph.SetPopulation(map.population, map.firstInfected, map.protagonist);
		GetParent().AddChild(graph);
		graph.GetNode<Camera2D>("./Camera2D").Current = true;
		
	}
}
