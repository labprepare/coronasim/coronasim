using Godot;
using System;

public class PauseMenu : Control
{



	// // Called when the node enters the scene tree for the first time.
	// public override void _Ready()
	// {

	// }

	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("pause"))
		{
			bool newPauseState = !GetTree().Paused;
			//pause/unpause the game
			GetTree().Paused = newPauseState;
			this.Visible = newPauseState;
			Time.Paused = newPauseState;
			GetViewport().GetNode<Control>("./Node2D/Map/CanvasLayer/DirectionalArrow").Visible = !newPauseState;
			GetViewport().GetNode<Control>("./Node2D/UILayer/Node2D/InfectedCounterDisplay").Visible = !newPauseState;
		}
	}


}
