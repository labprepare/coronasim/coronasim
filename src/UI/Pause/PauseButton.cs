using Godot;
using System;

public class PauseButton : Control
{
	private void _on_Button_pressed()
	{
		InputEventAction pauseEvent = new InputEventAction();
		pauseEvent.Action = "pause";
		pauseEvent.Pressed = true;
		Input.ParseInputEvent(pauseEvent);
	}
}
