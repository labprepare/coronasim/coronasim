using Godot;
using System;

public class DirectionalArrow : Control
{
	public Vector2 target = new Vector2(0, 0);
	public Person protagonist;
	private Node2D arrow;


	public override void _Ready()
	{


		arrow = GetNode<Node2D>("./Arrow");


	}
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		Vector2 currentPosition = protagonist.Position;
		float radianAngleToTarget = currentPosition.AngleToPoint(target);
		float angle = (float)(radianAngleToTarget * (180 / Math.PI));
		// GD.Print(String.Format("pos:{},rad:{},angle:{}",currentPosition,radianAngleToTarget,angle));
		arrow.RotationDegrees = angle;
	}
}
