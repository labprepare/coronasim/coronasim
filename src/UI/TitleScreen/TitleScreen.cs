
using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using Array = Godot.Collections.Array;


public class TitleScreen : Node2D
{

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode("Menue/Rows/StartGame").Connect("pressed", this, "StartGame");
		GetNode("Menue/Rows/LoadGame").Connect("pressed", this, "LoadGame");
		GetNode("Menue/Rows/AdditionalInfo").Connect("pressed", this, "AdditionalInfo");
		GetNode("Menue/Rows/Contribute").Connect("pressed", this, "Contribute");

	}

	public void _OnAcceptDialogConfirmed()
	{
		String text = GetNode<LineEdit>("AcceptDialog/LineEdit").Text;
		while (!text.IsValidInteger())
		{
			 text = text.Remove(0,1);
		}
		Util.SetRNG(text.ToInt());
		GetTree().ChangeScene("res://src/UI/Game.tscn");
	}

	public void StartGame()
	{
		GetNode<AcceptDialog>("AcceptDialog").Popup_();
		GetNode<LineEdit>("AcceptDialog/LineEdit").Text = DateTime.Now.Ticks.ToString();
	}

	public void LoadGame()
	{
		File saveGame = new File();
		if(saveGame.FileExists("user://save_game.save"))
		{
			AcceptDialog dialog = new AcceptDialog();
			dialog.DialogText = "A Previous save_gameFile does !exist. Please Start a new Game!";
			dialog.WindowTitle = "No save_gameFile!";
			dialog.Connect("modal_closed", dialog, "queue_free");
			AddChild(dialog);
			dialog.PopupCentered();
		}
		else
		{
			GetTree().ChangeScene("res://src/UI/LoadingScreen/LoadingScreen.tscn");
			GD.Print("blup");
	//		# save_game.open("user://save_game.save", File.READ)
	//		# while save_game.get_position() < save_game.get_len():
	//		# 	# Get the saved dictionary from the next line in the save file
	//		# 	var node_data = Godot.JSON.Parse(save_game.get_line());
	//
	//		# 	# Firstly, we need to create the object && add it to the tree && set its position.
	//		# 	var new_object = GD.Load(node_data["filename"]).instance();
	//		# 	get_node(node_data["parent"]).add_child(new_object)
	//		# 	new_object.position = new Vector2(node_data["pos_x"], node_data["pos_y"]);
	//
	//		# 	# Now we set the remaining variables.
	//		# 	for i in node_data.keys():
	//		# 		if i == "filename" || i == "parent" || i == "pos_x" || i == {"pos_y",
	//		# 			continue
	//		# 		new_object.set(i}, node_data[i])
		}
		saveGame.Close();
	}

	public void Settings()
	{
		GD.Print("Settings TODO");
	}

	public void AdditionalInfo()
	{
		GD.Print("additionalInfo TODO");
	}

	public void Contribute()
	{
		OS.ShellOpen("https://gitlab.com/labprepare/coronasim/coronasim");
	}



}
