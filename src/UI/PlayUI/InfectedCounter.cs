using Godot;
using System;
using System.Collections.Generic;

public class InfectedCounter: Button
{
	// Called every frame. 'delta' is the elapsed time since the previous frame.
    Population population;
    public InfectedCounter(){}
    public InfectedCounter(Population population) {
        this.population = population;
    }

	public override void _Process(float delta)
	{
        if ( population !=  null){
            List<Person> currentlyInfected = population.AllPeople.FindAll(x => x.IsInfectious() || x.IsRecovered() );
            // int count = 0;
            // if (currentlyInfected != null) {
            //     count = currentlyInfected.Count;
            // }
            Text = "Infiziert: " + currentlyInfected.Count.ToString();
        }
        else {
            this.population = GetViewport().GetNode<Map>("./Node2D/Map").population;
        }
	}
}
