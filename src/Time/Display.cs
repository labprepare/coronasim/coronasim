using Godot;
using System;

public class Display : Button
{
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		Text = Time.GetTime().ToString("dd.MM.yyyy - HH:mm");
	}
}
