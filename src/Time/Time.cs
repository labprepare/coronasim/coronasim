using System;
using Godot;
public static class Time
{
    private static double _MultiplierCache;
    public static bool Paused
    {
        get => Multiplier == 0;
        set
        {
            if (value)
            {
                if (!Paused)
                {
                    _MultiplierCache = Multiplier;
                    Multiplier = 0;
                }
            }
            else
            {
                if (Paused)
                {
                    Multiplier = _MultiplierCache;
                }
            }

        }
    }

    public static double Multiplier
    {
        get => _Multiplier;
        set
        {
            GetTime();
            _Multiplier = value;
        }
    }
    public const double MULTIPLIER = 1440;
    private static double _Multiplier = 1;
    private static DateTime lastTime = DateTime.Now;
    private static DateTime lastGameTime = DateTime.Now;

    public static DateTime GetTime()
    {
        DateTime now = DateTime.Now;
        TimeSpan timePassed = TimeSpan.FromMilliseconds((now - lastTime).TotalMilliseconds * Multiplier * MULTIPLIER);
        lastGameTime = lastGameTime.Add(timePassed);
        lastTime = now;
        return lastGameTime;
    }
    public static void SetGameTime(DateTime time)
    {
        lastGameTime = time;
    }
    public static void SetSpeedPause()
    {
        Multiplier = 0;
    }
    public static void SetSpeedNormal()
    {
        Multiplier = 1;
    }
    public static void SetSpeedFast()
    {
        Multiplier = 2;
    }
    public static void SetSpeedSuperFast()
    {
        Multiplier = 4;
    }
    public static void SetSpeedCustom(int i)
    {
        Multiplier = i;
    }
    public static void AdvanceTime(TimeSpan duration) {
        lastGameTime = lastGameTime.Add(duration);
    }
}
