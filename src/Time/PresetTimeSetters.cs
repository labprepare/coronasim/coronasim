using Godot;

public class PresetTimeSetters : Control
{
	private void _on_Pause_pressed()
	{
		Time.SetSpeedPause();
	}


	private void _on_Normal_pressed()
	{
		Time.SetSpeedNormal();
	}


	private void _on_Fast_pressed()
	{
		Time.SetSpeedFast();
	}


	private void _on_SuperFast_pressed()
	{
		Time.SetSpeedSuperFast();
	}
}
