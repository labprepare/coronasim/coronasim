using Godot;
using System;

public class TimeTracker : Control
{
    private DateTime lastTime;
    public override void _Ready()
    {
        lastTime = Time.GetTime();
    }
    public void SetLastTime(DateTime time) {
        lastTime = time;
    }
    public TimeSpan GetInterval(bool reset = true) {
        DateTime currentTime = Time.GetTime();
        TimeSpan timeSpan = currentTime - lastTime;
        if (reset) {
            lastTime = currentTime;
        }
        return timeSpan;
    }
    public DateTime GetTime() {
        return Time.GetTime();
    }
}