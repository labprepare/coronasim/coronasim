using Godot;
using System;

public class CustomTimeSetter : Control
{
	TextEdit textEdit;


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		textEdit = GetNode<TextEdit>("./TextEdit");
	}

	private void _on_Button_pressed()
	{
		int speed;
		try
		{
			speed = textEdit.Text.ToInt();
			Time.SetSpeedCustom(speed);
		}
		catch (System.Exception)
		{
			GD.Print("cant convert to int");
		}


	}
}
