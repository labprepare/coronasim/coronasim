using Godot;
using System.Collections.Generic;
using System.Diagnostics;
using System;


public static class Util
{
	public static Random RNG;

	public static Dictionary<Direction, List<Vector2>> GetDirectionalBorder(Vector2 start, Vector2 end, Direction direction = Direction.All)
	{
		Debug.Assert(start.x < end.x, "start.x < end.x did not hold");
		Debug.Assert(start.y < end.y, "start.y < end.y did not hold");

		Boolean North = (direction & Direction.North) != 0;
		Boolean South = (direction & Direction.South) != 0;
		Boolean East = (direction & Direction.East) != 0;
		Boolean West = (direction & Direction.West) != 0;

		List<Vector2> NorthEdge = new List<Vector2>();
		List<Vector2> SouthEdge = new List<Vector2>();
		List<Vector2> EastEdge = new List<Vector2>();
		List<Vector2> WestEdge = new List<Vector2>();


		Dictionary<Direction, List<Vector2>> border = new Dictionary<Direction, List<Vector2>>();
		for (int i = (int)start.x + 1; i < end.x; i++)
		{
			if (North) NorthEdge.Add(new Vector2(i, start.y));
			if (South) SouthEdge.Add(new Vector2(i, end.y));
		}
		for (int i = (int)start.y + 1; i < end.y; i++)
		{
			if (West) WestEdge.Add(new Vector2(start.x, i));
			if (East) EastEdge.Add(new Vector2(end.x, i));
		}

		if (North) border.Add(Direction.North, NorthEdge);
		if (East) border.Add(Direction.East, EastEdge);
		if (South) border.Add(Direction.South, SouthEdge);
		if (West) border.Add(Direction.West, WestEdge);

		return border;
	}

	public static List<Vector2> GetBorderCoordinates(Vector2 start, Vector2 end, Boolean corners = true, Direction direction = Direction.All)
	{
		Debug.Assert(start.x < end.x, "start.x < end.x did not hold");
		Debug.Assert(start.y < end.y, "start.y < end.y did not hold");
		List<Vector2> cords = new List<Vector2>();
		if (corners)
		{
			cords = new List<Vector2> { start, end, new Vector2(start.x, end.y), new Vector2(end.x, start.y) };
		}

		foreach (List<Vector2> edge in GetDirectionalBorder(start, end, direction).Values)
		{
			cords.AddRange(edge);
		}

		return cords;
	}
	public static List<Vector2> GetFilledCoordinates(Vector2 start, Vector2 end)
	{
		Debug.Assert(start.y <= end.y, "start.y <= end.y did not hold");
		Debug.Assert(start.x <= end.x, "start.x <= end.x did not hold");
		List<Vector2> cords = new List<Vector2>();
		for (int i = (int)start.x; i <= end.x; i++)
		{
			for (int j = (int)start.y; j <= end.y; j++)
			{
				cords.Add(new Vector2(i, j));
			}
		}
		return cords;

	}

	public static void SetRNG(int seed)
	{
		RNG = new Random(seed);
	}

	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = RNG.Next(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static List<T> ToList<T>(this T[] array)
	{
		List<T> tmp = new List<T>();
		foreach (T item in array)
		{
			tmp.Add(item);
		}
		return tmp;
	}

	public static T GetRandom<T>(this List<T> list) where T : class
	{
		if (list.Count == 0)
		{
			return null;
		}
		return list[RNG.Next(0, list.Count)];
	}
	public static Nullable<T> GetRandomN<T>(this List<T> list) where T : struct
	{
		if (list.Count == 0)
		{
			return null;
		}
		return list[RNG.Next(0, list.Count)];
	}

	public static T PopFirst<T>(this List<T> list) where T : class
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentFirst = list[0];
		list.RemoveAt(0);
		return (T)currentFirst;
	}
	public static Nullable<T> PopFirstN<T>(this List<T> list) where T : struct
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentFirst = list[0];
		list.RemoveAt(0);
		return (T)currentFirst;
	}

	public static T PopLast<T>(this List<T> list) where T : class
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[list.Count - 1];
		list.RemoveAt(list.Count - 1);
		return currentLast;
	}
	public static Nullable<T> PopLastN<T>(this List<T> list) where T : struct
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[list.Count - 1];
		list.RemoveAt(list.Count - 1);
		return currentLast;
	}
	public static T GetFirst<T>(this List<T> list) where T : class
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[0];
		return currentLast;
	}
	public static Nullable<T> GetFirstN<T>(this List<T> list) where T : struct
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[0];
		return currentLast;
	}
	public static T GetLast<T>(this List<T> list) where T : class
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[list.Count - 1];
		return currentLast;
	}
	public static Nullable<T> GetLastN<T>(this List<T> list) where T : struct
	{
		if (list == null || list.Count == 0)
		{
			return null;
		}
		T currentLast = list[list.Count];
		return currentLast;
	}

	///Custom Getter that returns a default value instead of throwing KeyNotFound Exceptions
	public static TV GetValue<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV defaultValue = default(TV))
	{
		TV value;
		return dict.TryGetValue(key, out value) ? value : defaultValue;
	}

	public static Dictionary<TK, TOutput> ConvertAll<TK, TV, TOutput>(this IDictionary<TK, TV> dict, Converter<TV, TOutput> converter)
	{
		Dictionary<TK, TOutput> tmp = new Dictionary<TK, TOutput>();
		foreach (KeyValuePair<TK, TV> item in dict)
		{
			tmp.Add(item.Key, converter.Invoke(item.Value));
		}
		return tmp;
	}

}
