using System;
using Godot;
using System.Collections.Generic;


public class Population : Node
{

    public const int POPULATION_SIZE = 1500;

    public List<Person> AllPeople = new List<Person>();
    public List<String> AllTags = new List<String>();
    public List<Company> Companies = new List<Company>();
    public List<Apartment> Apartments;

    public override void _Ready()
    {

        PopulationGenerator generatedPopulation = new PopulationGenerator(POPULATION_SIZE);
        AllPeople = generatedPopulation.AllPeople;
        AllTags = generatedPopulation.AllTags;
        Companies = generatedPopulation.Companies;
        Apartments = generatedPopulation.Apartments;
        // TODO Friends
        foreach (Person person in AllPeople)
        {
            AddChild(person);
            person.Name = GD.Str(person.GetId()) + "_" + person.FirstName + "_" + person.LastName;
        }

    }

    public Person GetProtagonist()
    {
        AllPeople.GetLast().PersonIsProtagonist = true;
        return AllPeople.GetLast();
    }

    public List<Apartment> GetAppartmentAssignments()
    {
        return Apartments;
    }

    public void AddTag(String tag)
    {
        switch (AllTags.FindAll(x => x.Equals(tag)).Count)
        {
            case 0:
                AllTags.Add(tag);
                break;
            case 1:
                break;
            default:
                GD.PushError("all_tags contains" + tag + "more than once");
                break;
        }
    }



}
