
using System;
using Godot;
using System.Collections.Generic;


public class Company : Godot.Object
{
	public String Name;
	public String Type;
	public List<Person> Staff;
	public Person Boss;

	public Company(String companyName, String companyType, List<Person> staff, Person boss)
	{
		Name = companyName;
		Type = companyType;
		Staff = staff;
		Boss = boss;
	}
}