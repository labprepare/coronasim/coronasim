using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using System.Collections.Generic;
using MathNet.Numerics.Distributions;

public class Person : Node2D
{
	//is part of the scene person

	public float Speed = 0.3f;
	private Map Map;
	InfectiousBubble InfectiousBubble;
	public List<Infection> TransmittedInfections = new List<Infection>(); // this -> Alice
	public List<Infection> OwnInfections = new List<Infection>(); // (Bob -> this)
																  // public List<Schedule> Schedule =new Array(){};
	public List<Vector2> Path = new List<Vector2>();
	public List<Line2D> GraphLines = new List<Line2D>();
	public Vector2 Velocity = new Vector2();
	public bool Alive = true;
	public GraphPerson graphPerson;
	public string WorkplaceName = "Arbeitslos";
	private DateTime? walkAgainTime;
	private Normal delayUntilWalkAgainDistribution = new Normal(4, 1, Util.RNG);

	//temp
	public List<String> Tags = new List<String>();

	public List<Relation> Relations = new List<Relation>();

	public String FirstName;
	public String LastName;
	public int Age;
	public Dictionary AssignedBuildings = new Dictionary();
	public Boolean PersonIsProtagonist = false;
	private int id;
	private TimeTracker timeTracker = new TimeTracker();
	public Person(){}
	public Person(int identifier, String firstName, String lastName)
	{
		id = identifier;
		FirstName = firstName;
		LastName = lastName;
	}

	public override void _Ready()
	{
		Map = (Map)GetParent().GetParent();//.GetNode<TileMap>("./Navigation2D/TileMap");
		InfectiousBubble = GetNode<InfectiousBubble>("./InfectiousBubble");
	}
	public void SetValues(int identifier, String firstName, String lastName)
	{
		id = identifier;
		FirstName = firstName;
		LastName = lastName;
	}
	public int GetId()
	{
		return id;
	}
	public void SetId(int identifier)
	{
		id = identifier;
	}

	public void SetPath(Vector2[] path)
	{
		Path = path.ToList();
	}

	public override void _PhysicsProcess(float delta)
	{
		bool goOn = true;
		if (PersonIsProtagonist)
		{
			if (Map.playerControl)
			{
				goOn = false;
			}
		}
		if (Path.Count == 0 && goOn)
		{
			if (walkAgainTime==null)
			{
				TimeSpan delay = TimeSpan.FromHours(delayUntilWalkAgainDistribution.Sample());
				walkAgainTime = Time.GetTime().Add(delay);
			}

			if (Time.GetTime()>=walkAgainTime)
			{
				walkAgainTime = null;
				SetPath(Map.navigation.GetSimplePath(Position, GetNextTarget(), optimize: false));
			}

		}

		// Calculate the movement distance for this frame
		double timePassed = timeTracker.GetInterval().TotalSeconds;
		float distanceToWalk = Speed * (float) timePassed;
		TileMap TileMap = Map.tilemap;
		// Move the player along the path until he has run out of movement || the path ends.
		while (distanceToWalk > 0 && Path.Count > 0)
		{
			Vector2 oldPos = Position;
			float distanceToNextPoint = this.Position.DistanceTo(Path[0]);
			if (distanceToWalk <= distanceToNextPoint)
			{
				// The player does !have enough movement left to get to the next point.
				this.Position += this.Position.DirectionTo(Path[0]) * distanceToWalk;
			}
			else
			{
				// The player get to the next point
				this.Position = Path[0];
				Path.PopFirstN();

			}

			//get the tilemap the person is standing on
			Vector2 oldTile = TileMap.WorldToMap(oldPos);
			Vector2 newTile = TileMap.WorldToMap(Position);
			if (oldTile != newTile)
			{
				TileTransition(oldTile, newTile);
			}

			// Update the distance to walk
			distanceToWalk -= distanceToNextPoint;
		}

	}


	public void AddTag(String tag)
	{
		switch (Tags.FindAll(x => x.Equals(tag)).Count)
		{
			case 0:
				Tags.Add(tag);
				break;
			case 1:
				break;
			default:
				GD.PushError("tags contains" + tag + "more than once");
				break;
		}
	}

	//specifies if a person is currently infected with a disease
	public bool IsInfectious()
	{
		return OwnInfections.Exists(i => i.IsContagious());
	}
	public bool IsRecovered() {
		return OwnInfections.Exists(i => i.IsRecovered());
	}

	//gets the diseases this person is currently spreading.
	public List<Infection> GetSpreadingInfections()
	{
		return OwnInfections.FindAll(i => i.IsContagious());
	}

	public List<Disease> GetSpreadingDiseases()
	{
		return GetSpreadingInfections().ConvertAll(i => i.Disease);
	}

	public Infection GetInfection(Disease disease)
	{
		//find last because we want the most recent instance of that infection
		return OwnInfections.FindLast(i => i.Disease == disease);
	}

	public bool IsSusceptibleAgainst(Disease disease)
	{
		//if null then person was not yet infected and is therefore not immune
		return !OwnInfections.Exists(i => i.providesImmunity(disease));
		//return GetInfection(disease) == null;
	}

	public Vector2 GetNextTarget()
	{
		Normal gaussian = new Normal(0, 500, Util.RNG);
		return Position + new Vector2(Convert.ToSingle(gaussian.Sample()),Convert.ToSingle(gaussian.Sample()));
	}

	public void TileTransition(Vector2 fromTileMapPosition, Vector2 toTileMapPosition)
	{

		// determine from what tile the person went into what other tile
		IMapObject from = Map.GetMapObject(fromTileMapPosition);
		IMapObject to = Map.GetMapObject(toTileMapPosition);

		//execute the logic
		//requires IMapObject to implement this properly (Buildings are only equal, when it is the same building. All outdoor things(Parks and Streets) are Equal IMapObjects)
		if (from != null ? !from.Equals(to) : to != null)
		{

			if (from != null)
			{
				if (from is Building)
				{
					//asu from austreten
					Building fromB = (Building)from;
					fromB.PersonLeaves(this);
				}
				else
				{
					//not in a building, so the person was outside (delete the own Infectious Bubble/step out of other infectious bubbles)
					InfectiousBubble.Active = false;
				}
			}

			if (to != null)
			{
				//Person enters a building so it becomes part of the simulation for that buiding
				if (to is Building)
				{
					Building toB = (Building)to;
					toB.PersonEnters(this);
				}
				else
				{
					InfectiousBubble.Active = true;
				}
			}
		}
	}
}
