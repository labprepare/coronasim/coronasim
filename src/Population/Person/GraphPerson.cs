using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using System.Collections.Generic;
using MathNet.Numerics.Distributions;

public class GraphPerson : GraphNode
{
    public Person originPerson;
    public Polygon2D polygon;
    public Node2D node;
    public Button showInfectedButton;
    public TextEdit infoText;
    public bool showInfected = false;
    public bool showInfo = false;
    public bool showInfectors = false;

    public override void _Ready()
    {
        Title = originPerson.FirstName + " " + originPerson.LastName;
        node = GetChild<Node2D>(0);
        showInfectedButton = node.GetNode<Button>("./showInfected");
        infoText = node.GetNode<TextEdit>("./TextEdit");

        originPerson.RemoveChild(polygon);
        node.GetNode<Node2D>("./PolygonNode").AddChild(polygon);
        SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(0, 1, 0, 1));

        infoText.SetLine(0, "Alter: " + originPerson.Age);
        infoText.SetLine(1, "Arbeit: " + originPerson.WorkplaceName);
        if (originPerson.Alive)
        {
            infoText.SetLine(2, "Gesund: Krank");
        }
        else
        {
            infoText.SetLine(2, "Gesund: Tot");
        }
        infoText.Visible = false;
    }
    public void SetGraphPerson(Person person)
    {
        originPerson = person;
        person.graphPerson = this;
        polygon = person.GetNode<Polygon2D>("./Polygon2D");
    }
    public void _onShowInfoPressed()
    {
        if (showInfo)
        {
            infoText.Visible = false;
            polygon.Visible = true;
            showInfo = false;
        }
        else
        {
            infoText.Visible = true;
            polygon.Visible = false;
            showInfo = true;
        }

    }
    public void _onShowInfectorsPressed()
    {
        if (showInfectors)
        {
            GetParent<GraphVisualization>().ResetLines();
            ShowInfectionWay(false);
            SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(0, 1, 0, 1));
            showInfectors = false;
        }
        else
        {
            GetParent<GraphVisualization>().ResetLines();
            ShowInfectionWay(true);
            SetSlot(0, true, 0, new Color(1, 0, 0, 1), true, 0, new Color(0, 1, 0, 1));
            showInfectors = true;
        }
    }
    public void _onShowInfectedPressed()
    {
        if (showInfected)
        {
            GetParent<GraphVisualization>().ResetLines();
            ReverseShowInfected();
        }
        else
        {
            GetParent<GraphVisualization>().ResetLines();
            SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(1, 0, 0, 1));
            foreach (Infection infection in originPerson.TransmittedInfections)
            {
                infection.PersonB.graphPerson.SetSlot(0, true, 0, new Color(1, 0, 0, 1), true, 0, new Color(0, 1, 0, 1));
            }
            showInfected = true;
        }
        
    }
    public void ReverseShowInfected()
    {
        SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(0, 1, 0, 1));
        foreach (Infection infection in originPerson.TransmittedInfections)
        {
            infection.PersonB.graphPerson.SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(0, 1, 0, 1));
        }
        showInfected = false;
    }
    public void ResetPolygon()
    {
        polygon.Visible = true;
        node.GetNode<Node2D>("./PolygonNode").RemoveChild(polygon);
        originPerson.AddChild(polygon);
    }
    public void ShowInfectionWay(bool show)
    {
        Person infector = originPerson.OwnInfections[0].PersonA;
        Color color = new Color(0, 1, 0, 1);
        if (show)
        {
            color = new Color(1, 0, 0, 1);
        }
        if (infector != null)
        {
            infector.graphPerson.SetSlot(0, true, 0, color, true, 0, color);
            infector.graphPerson.ShowInfectionWay(show);
        }
    }
}
