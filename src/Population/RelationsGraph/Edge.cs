
using Godot;
using Array = Godot.Collections.Array;


public class Edge : Node
{

	public Person PersonA;
	public Person PersonB;

	public Edge(Person source, Person destination)
	{
		PersonA = source;
		PersonB = destination;
	}

	public Array GetPersons()
	{
		return new Array( this.PersonA, this.PersonB);
	}
}