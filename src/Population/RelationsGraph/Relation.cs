using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using System.Collections.Generic;

//fRelation test = new Relation(src, trgt).AddRelationKind(xy).AddRelationKind(yz);


public class Relation : Edge
{
    [Flags]
    public enum RelationType
    {
        COWORKER = 1 << 0,
        SPOUSE = 1 << 1,
        FAMILY = 1 << 2,
        FRIEND = 1 << 3
    }
    private RelationType type;
    public Relation(Person source, Person target) : base(source, target) { }


    public Relation AddRelation(RelationType relationType)
    {
        type |= relationType;
        return this;
    }

    public Relation RemoveRelation(RelationType relationType)
    {
        // bitwise and with the complement
        type &= ~relationType;
        return this;
    }

    public bool IsRelationType(RelationType relationType)
    {

        return (type | relationType) != 0;
    }




}