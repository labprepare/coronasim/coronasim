using Godot;
using System;
using System.Collections.Generic;

public class LivingComunity
{
    public List<Person> People = new List<Person>();
    public Vector2 House;

    public LivingComunity (Vector2 house, List<Person> people) {
        House = house;
        People = people;
    }
}