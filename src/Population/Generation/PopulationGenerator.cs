
using System;
using Godot;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.Distributions;


public class PopulationGenerator : Godot.Object
{

    public System.Random rng = Util.RNG;
    public List<Person> AllPeople = new List<Person>();
    public List<String> AllTags = new List<String>();
    public List<Company> Companies = new List<Company>();
    public List<Apartment> Apartments = new List<Apartment>();
    private CompanyGenerator companyGenerator;
    private NameGenerator nameGenerator;

    public PopulationGenerator(int size)
    {
        companyGenerator = new CompanyGenerator();
        nameGenerator = new NameGenerator();
        GeneratePopulation(size);
        GenerateJobs();
    }

    public void GeneratePopulation(int populationSize)
    {
        AllPeople = new List<Person>();
        while ((AllPeople.Count < populationSize))
        {
            int size = rng.Next(1, 9);
            List<Person> family = GenerateFamily(size);

            AddToPopulation(family);

        }
    }

    public void GenerateJobs()
    {
        AddTag("Boss");
        List<Person> people = new List<Person>(AllPeople);
        Companies = new List<Company>();
        while (people.Count != 0)
        {
            int companySize = rng.Next(1, 21);
            if (companySize > people.Count)
            {
                companySize = people.Count;
            }
            List<Person> company_worker = new List<Person>();
            foreach (int _i in Enumerable.Range(1, companySize))
            {
                Person worker = people.GetRandom();
                company_worker.Add(worker);
                people.Remove(worker);
            }
            Company company = AddHierarchyToWorker(company_worker);
            Companies.Add(company);
        }
    }

    public Company AddHierarchyToWorker(List<Person> worker)
    {
        Person boss = worker.GetRandom<Person>();

        (string name,string type) = companyGenerator.GetCompanyNameAndType(boss.LastName);

        Company company = new Company(name, type, worker, boss);
        AddTag(type);
        AddTag(name);// !collisionfree
        AddTagsToCompany(company);

        foreach (Person person in worker)
        {
            person.WorkplaceName = name;
        }

        return company;
    }

    public void AddTagsToCompany(Company company)
    {
        foreach (Person i in company.Staff)
        {
            i.AddTag(company.Name);
            i.AddTag(company.Type);
        }
    }

    public List<Person> GenerateFamily(int size)
    {
        PackedScene PersonScene = (PackedScene)ResourceLoader.Load("res://src/Population/Person/Person.tscn");
        List<Person> persons = new List<Person>();
        string lastName = nameGenerator.GetNewRandomLastName();
        for (int i = 0; i < size; i++)
        {
            string firstName = nameGenerator.GetNewRandomFirstName();
            Person newPerson = PersonScene.Instance<Person>();
            newPerson.SetValues(i, firstName, lastName);
            persons.Add(newPerson);
        }
        GenerateCouples(persons);
        GenerateAge(persons);
        AddRelation(persons, Relation.RelationType.FAMILY);
        return persons;

    }

    public void GenerateCouples(List<Person> people)
    {
        double couples = people.Count * 0.6;
        for (int i = 0; i < (int)couples; i++, i++)
        {
            AddRelation(new List<Person>(){people[i], people[i + 1]}, Relation.RelationType.SPOUSE);
		}
    }

public void GenerateAge(List<Person> people)
{
	foreach (Person person in people)
	{
		if (person.Age != default(int)) { continue; }
		int percentage = rng.Next(0, 101);
        int from = 0;
        int to = 0;
		if (person.Relations.Count == 0)
        {
            // use aprox percentage from "Statistische Bundesamt"
            if ((percentage <= 2))
            {
                from = 0;
                to = 3;
            }
            if (2 < percentage && percentage <= 5)
            {
                from = 3;
                to = 6;
            }
            if (5 < percentage && percentage <= 13)
            {
                from = 6;
                to = 14;
            }
            if (13 < percentage && percentage <= 16)
            {
                from = 15;
                to = 17;
            }
            if (16 < percentage && percentage <= 24)
            {
                from = 18;
                to = 24;
            }
            if (24 < percentage && percentage <= 30)
            {
                from = 25;
                to = 29;
            }
            if (30 < percentage && percentage <= 42)
            {
                from = 30;
                to = 39;
            }
            if (42 < percentage && percentage <= 59)
            {
                from = 40;
                to = 49;
            }
            if (59 < percentage && percentage <= 79)
            {
                from = 50;
                to = 64;
            }
            if (79 < percentage && percentage <= 90)
            {
                from = 65;
                to = 74;
            }
            if ((90 < percentage))
            {
                from = 75;
                to = 100;
            }
            person.Age = rng.Next(from, to+1);
        }
        else
        {
            // prevent to young spouses
            if (0 < percentage && percentage <= 24)
            {

                from = 18;
                to = 24;
            }
            if (24 < percentage && percentage <= 30)
            {
                from = 25;
                to = 29;
            }
            if (30 < percentage && percentage <= 42)
            {
                from = 30;
                to = 39;
            }
            if (42 < percentage && percentage <= 59)
            {
                from = 40;
                to = 49;
            }
            if (59 < percentage && percentage <= 79)
            {
                from = 50;
                to = 64;
            }
            if (79 < percentage && percentage <= 90)
            {
                from = 65;
                to = 74;
            }
            if ((90 < percentage))
            {
                from = 75;
                to = 100;
            }
            person.Age = rng.Next(from, to+1);

            Normal gaussian = new Normal(person.Age,0.1*person.Age,Util.RNG);

            Person spouse;
            if (person != person.Relations[0].PersonA) {
            	spouse = person.Relations[0].PersonA;
			}
			else {
				spouse = person.Relations[0].PersonB;
			}
			// Normalverteilte zufallsveriable mean ist altes alter diveriance ist alter spouse * 0,8
            spouse.Age = Convert.ToInt32(Math.Round(gaussian.Sample()));
        }
	}
    IEnumerable<int> people_ids = GD.Range(0, people.Count);

}

public void AddRelation(List<Person> people, Relation.RelationType type)
{
    if ((people.Count == 0))
    {
        GD.PushError("_add_relation Array Error is empty");
    }
    foreach (Person personA in people)
    {
        foreach (Person personB in people)
        {
            if (personA != personB)
            {
                personA.Relations.Add(new Relation(personA, personB).AddRelation(type));
                personB.Relations.Add((new Relation(personB, personA).AddRelation(type)));
            }

        }
    }
}

public void AddToPopulation(List<Person> people)
{
    int old_index = AllPeople.Count;
    Apartment living_community = new Apartment(people);
	foreach (Person person in living_community.People)
	{
		person.SetId(old_index + person.GetId());
        AllPeople.Add(person);
	}
    Apartments.Add(living_community);

}

public void AddTag(String tag)
	{
		switch (AllTags.FindAll(x => x.Equals(tag)).Count)
		{
			case 0:
				AllTags.Add(tag);
				break;
			case 1:
				break;
			default:
				GD.PushError("all_tags contains" + tag + "more than once");
				break;
		}
	}



}