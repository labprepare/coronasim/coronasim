using System;
using System.Collections.Generic;
using Godot;
using Array = Godot.Collections.Array;
using Newtonsoft.Json;


public class NameGenerator
{
    private List<String> firstNames;
    private List<String> lastNames;

    public NameGenerator()
    {
        firstNames = LoadFromJSON("res://src/Population/Generation/Names/First_Names.json");
        lastNames = LoadFromJSON("res://src/Population/Generation/Names/Last_Names.json");
    }


public List<String> LoadFromJSON(String path)
	{
		File file = new File();
		file.Open(path, File.ModeFlags.Read);
		string tmp_data = file.GetAsText();
		file.Close();

		return JsonConvert.DeserializeObject<List<String>>(tmp_data);
	}

    public String GetNewRandomFirstName()
    {
        return firstNames.GetRandom();
    }

    public String GetNewRandomLastName()
    {
        return lastNames.GetRandom();


    }
}