
using System;
using Godot;
using Array = Godot.Collections.Array;
using System.Collections.Generic;
using Newtonsoft.Json;


public class CompanyGenerator
{
	public class CompanyType {
		public String type;
		public String prefix;
		public String suffix;
	}
	private List<CompanyType> companyTypes;

	public CompanyGenerator()
	{
		companyTypes = LoadFromJSON("res://src/Population/Generation/Companies.json");
	}

	public List<CompanyType> LoadFromJSON(String path)
	{
		File file = new File();
		file.Open(path, File.ModeFlags.Read);
		string tmp_data = file.GetAsText();
		file.Close();

		return JsonConvert.DeserializeObject<List<CompanyType>>(tmp_data);
	}

	public (String, String) GetCompanyNameAndType(String lastname)
	{
		CompanyType companyType = companyTypes.GetRandom();
		string companyName = companyType.prefix + " " + lastname + " " + companyType.suffix;
		return (companyName, companyType.type);
	}
}