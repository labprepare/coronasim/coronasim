using System;
using Godot;
using Dictionary = Godot.Collections.Dictionary;
using Array = Godot.Collections.Array;
using System.Collections.Generic;

public class GraphVisualization: GraphEdit
{
	public Population population;
	public Person person;
	public Person protagonist;
	public List<Vector2> vectors;
	public List<Person> infected;
	public List<GraphPerson> graphPeople;
	public DateTime time;
	public PackedScene infectedPersonScene;
	public Button resetButton;
	public long timeFirstInfection;
	public const int personHeight = 100;


	public override void _Ready()
	{
		resetButton = GetNode<Button>("./ResetButton");
	}
	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("zoom_in"))
		{
			Zoom -= (float) 0.5;
		}
		if (@event.IsActionPressed("zoom_out"))
		{
			Zoom += (float) 0.5;
		}
	}
	public void _onResetButtonPressed()
	{
		Reset(person);
	}
    public void SetPopulation(Population pop, Person firstInfected, Person mainPerson)
    {
		population = pop;
		person = firstInfected;
		protagonist = mainPerson;
		timeFirstInfection = person.OwnInfections[0].TimeOfInfection.Ticks;
		ShowGraph(person);
    }

	public void ShowGraph(Person person)
	{
		infected = new List<Person>();
		vectors = new List<Vector2>();
		graphPeople = new List<GraphPerson>();
		BuildGraph(new List<Person>(){person});
	}
	public void BuildGraph(List<Person> persons)
	{
		List<Person> personList = new List<Person>();
		infectedPersonScene = (PackedScene)ResourceLoader.Load("res://src/Population/Person/GraphPerson.tscn");

		foreach(Person person in persons)
		{
			GraphPerson infectedPerson = infectedPersonScene.Instance<GraphPerson>();
			Person infector = person.OwnInfections[0].PersonA;
			infectedPerson.SetGraphPerson(person);
			graphPeople.Add(infectedPerson);

			if(infector != null)
			{
				AddChild(infectedPerson);
				long timeOwnInfection = person.OwnInfections[0].TimeOfInfection.Ticks;
				float xPosition = (timeOwnInfection - timeFirstInfection)* 25/TimeSpan.TicksPerHour;
				float yPosition = FindYPosition(vectors, xPosition, infector.graphPerson.Offset);
				infectedPerson.Offset = new Vector2(xPosition, yPosition);

				ConnectNode(infector.graphPerson.Name,0,infectedPerson.Name,0);
				vectors.Add(infectedPerson.Offset);
			}
			else
			{
				vectors.Add(infectedPerson.Offset);
				AddChild(infectedPerson);
			}
			foreach(Infection i in person.TransmittedInfections)
			{
				if(i.PersonB != person)
				{
					personList.Add(i.PersonB);
				}
			}
		}
		if(personList.Count > 0)
		{
			BuildGraph(personList);
		}
	}

	public float FindYPosition(List<Vector2> vectors, float xPosition, Vector2 infectorGlobalPosition)
	{
		float yPosition = infectorGlobalPosition.y;
		List<float> takenYPositions = new List<float>();
		foreach (Vector2 vector in vectors)
		{
			if (Math.Abs(vector.x - xPosition) < 300)
			{
				takenYPositions.Add(vector.y);
			}
			if (yPosition == vector.y)
			{
				yPosition += (float)(Math.Pow(-1, vectors.Count) * 20);
			}
		}
		bool notFound = true;
		int n = 1;
		while (notFound)
		{
			notFound = false;
			foreach (float takenY in takenYPositions)
			{
				if (Math.Abs(takenY - yPosition) < personHeight)
				{
					yPosition += (float)(Math.Pow(-1, n) * 50 * n);
					n += 1;
					notFound = true;
					break;
				}
			}
		}
		return yPosition;
	}
	public void Reset(Person firstInfected)
	{
		foreach (GraphPerson graphPerson in graphPeople)
		{
			graphPerson.ResetPolygon();
		}
		protagonist.GetNode<Camera2D>("./Camera2D").Current = true;
		List<Vector2> vector = new List<Vector2>();
		protagonist.Path = vector;
		resetButton.RemoveAndSkip();
		RemoveAndSkip();
	}
	public void ResetLines()
	{
		foreach (GraphPerson graphPerson in graphPeople)
		{
			graphPerson.showInfectors = false;
			graphPerson.showInfected = false;
			graphPerson.SetSlot(0, true, 0, new Color(0, 1, 0, 1), true, 0, new Color(0, 1, 0, 1));
		}
	}
}